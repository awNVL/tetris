/* Container - header */
#ifndef CONTAINER_H
#define CONTAINER_H
#include <vector>
using std::vector;

class Container{
	public:
		Container();
		
		void comeIn(int gIndex=1,int sta=0);						//Add a new graph to container
		void show();
		void elimination();															//Check if rows can be eliminated
		bool overJudge();																//Check if the game is over
	
	private:
		void initialPosition();													//Initialize the base position each time when a new graph come in
		bool isBottom();																//Check if arrived bottom
		void cleanActive();															//Before a new graph come in,clean all the active grids
		void stableSetUp();															//Cannot keep falling,so set the active graph stable
		bool isActive(int rIndex,int cIndex);						//Check if a position is active
		bool isStable(int rIndex,int cIndex);						//Check if a position is stable
		bool outOfRange(int rIndex,int cIndex);					//Check if a position is over the boundary
		void speedUp();																	//Increase down speed of the graph in the container
		void scoreIncrease();														//Increase the score
			
		void staticShow();															//Show the current position of the graph
		void g1Active();
		void g2Active();
		void g3Active();
		void g4Active();
		void g5Active();
		void g6Active();
		void g7Active();
		
		void getCommand(int ck2);												//Receive the keyboard command
		
		void leftToMove();
		void rightToMove();
		
		void upToTransform();
		void g1Up();
		void g2Up();
		void g3Up();
		void g4Up();
		void g5Up();
		void g6Up();
		void g7Up();
		
		void natureDown();															//Down naturally
		bool keepFalling();															//Check if the graph should keep falling or not	
		void spaceToFallDirectly();											//Down directly
		int findDownRow();															//Find the row the base position should be after falling									
		void g1Stable();
		void g2Stable();
		void g3Stable();
		void g4Stable();
		void g5Stable();
		void g6Stable();
		void g7Stable();
		
		vector< vector<int> > box;											//Construct a two - dimension array
		int graphIndex;																	//Current graph's index
		int status;																			//Current graph's status
		int row;																				//The active graph's current row
		int col;																				//The active graph's current column	
		int score;																			//Score increase when a row is eliminated
		int times;																			//Record how many graphs came in,to judge if the container need to increase down speed
		int speedFactor;																//Current down speed of the graph in the container
		enum {BLANK=0,ACTIVE=1,STABLE=2};								//To output the corresponding symbol in each grid of the container									
};
#endif
