/* Tetris - main */
#include <iostream>
#include <ctime>
#include <cstdlib>
#include "Container.h"
using std::cin;
using std::cout;
using std::endl;

const int GRAPHLIMIT=7;
const int STATUS1=1;
const int STATUS2=2;
const int STATUS4=4;

int randomGraph(int limit);
int randomStatus(int limit);

int main(){
	std::srand(std::time(0));
	Container vessel;
	
	for(time_t now=clock();clock()-now<CLOCKS_PER_SEC;	);
	cout<<"Waiting to begin"<<endl;
	for(time_t now=clock();clock()-now<CLOCKS_PER_SEC*3;	);
	
	while(vessel.overJudge()==false){
		vessel.elimination();
		int index=randomGraph(GRAPHLIMIT);
		int statusAmounts=0;
		switch(index){
			case 1:case 2:case 7:
				statusAmounts=randomStatus(STATUS4);
				break;
			case 4:case 5:case 6:
				statusAmounts=randomStatus(STATUS2);
				break;
			case 3:
				statusAmounts=randomStatus(STATUS1);
				break;
		}
		vessel.comeIn(index,statusAmounts);
		vessel.show();
	}
	cout<<endl;
	cout<<"                                 OVER"<<endl;
	cout<<"Press any key to end."<<endl;
	char x;
	cin>>x;
	return 0;
}

int randomGraph(int limit){
	return 1+randomStatus(limit);
}

int randomStatus(int limit){
	return static_cast<int>(limit*(std::rand()/(RAND_MAX+1.0)));
}
