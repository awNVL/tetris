/* Container - definition */
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <conio.h>
#include <windows.h>
#include "Container.h"
using std::cout;
using std::endl;

const int ROWSIZE=21;
const int COLSIZE=17;
const int INIROW=0;
const int INICOL=9;
const int TIMESTOSPEEDUP=25;
const char GRID=2;
const char BORDER='|';

void backToBegin();

/* Construct a two - dimension array */
Container::Container():box(ROWSIZE),graphIndex(0),status(0),row(INIROW),col(INICOL),score(0),times(0),speedFactor(1){
	for(int i=0;i<ROWSIZE;i++)
		box[i].resize(COLSIZE);
		
	for(int i=0;i<ROWSIZE;i++)												//Initialization
		for(int j=0;j<COLSIZE;j++)
			box[i][j]=BLANK;
}

/* Initialize the base position each time when a new graph come in */
void Container::initialPosition(){
	col=INICOL;
	
	if(graphIndex==1||graphIndex==2||graphIndex==7){
		switch(status){
			case 0:case 2:
				row=INIROW+1;
				break;
			case 1:case 3:
				row=INIROW+2;
				break;
		}
	}
	if(graphIndex==3)
		row=INIROW+1;
	if(graphIndex==4||graphIndex==5){
		switch(status){
			case 0:
				row=INIROW+2;
				break;
			case 1:
				row=INIROW+1;
				break;
		}
	}
	if(graphIndex==6){
		switch(status){
			case 0:
				row=INIROW;
				break;
			case 1:
				row=INIROW+3;
				break;
		}
	}
}

void Container::comeIn(int gIndex,int sta){
	/* Don't forget when a new graph come in,reset the row and column as the entrance position */
	/* And increase the times of the graph came in */
	++times;
	if(times%TIMESTOSPEEDUP==0)												//Speed up when we have enough times
		speedUp();
	graphIndex=gIndex;
	status=sta;
	
	initialPosition();
}

bool Container::isBottom(){
	return row==ROWSIZE-1;
}

void Container::cleanActive(){
	for(int i=0;i<ROWSIZE;i++)
		for(int j=0;j<COLSIZE;j++)
			if(isActive(i,j))
				box[i][j]=BLANK;
}

void Container::stableSetUp(){
	switch(graphIndex){
			case 1:
				g1Stable();
				break;
			case 2:
				g2Stable();
				break;
			case 3:
				g3Stable();
				break;
			case 4:
				g4Stable();
				break;
			case 5:
				g5Stable();
				break;
			case 6:
				g6Stable();
				break;
			case 7:
				g7Stable();
				break;
		}
}

bool Container::isActive(int rIndex,int cIndex){
	return box[rIndex][cIndex]==ACTIVE;
}

bool Container::isStable(int rIndex,int cIndex){
	return box[rIndex][cIndex]==STABLE;
}

bool Container::outOfRange(int rIndex,int cIndex){
	return rIndex<0||rIndex>=ROWSIZE||cIndex<0||cIndex>=COLSIZE;
}

void Container::speedUp(){
	++speedFactor;
}

void Container::elimination(){
	/* Check from bottom to top */
	int count=0;
	for(int i=ROWSIZE-1;i>-1;--i){
		count=0;
		for(int j=0;j<COLSIZE;++j){
			if(isStable(i,j))
				++count;
		}
		if(count==COLSIZE){												
			/* Eliminate this row */
			for(int k=0;k<COLSIZE;++k)
				box[i][k]=BLANK;
			
			/* And then move all stable grids to fall down a row */
			/* Search from bottom to top to avoid the situation which two grids are successive */
			for(int tr=i-1;tr>-1;--tr)
				for(int tc=0;tc<COLSIZE;++tc)
					if(isStable(tr,tc)){
						box[tr][tc]=BLANK;
						box[tr+1][tc]=STABLE;
					}
			/* Stable grids fallen,check from this row again */
			++i;
			/* Increase the score after a row is eliminated*/
			scoreIncrease();												
		}
	}
	for(time_t now=clock();clock()-now<CLOCKS_PER_SEC/2;	);
}

void Container::scoreIncrease(){
	score+=10*speedFactor;
}

bool Container::overJudge(){
	for(int oc=0;oc<COLSIZE;++oc)
		if(isStable(INIROW,oc))
			return true;
	
	return false;
}

void Container::getCommand(int ck2){
	if(ck2==0x48)
		upToTransform();
	if(ck2==0x50)
		natureDown();
	if(ck2==0x4b)
		leftToMove();
	if(ck2==0x4d)
		rightToMove();
}

void Container::leftToMove(){
	/* Reverse search from top to bottom,left to right */
	/* To avoid the situation which two grid are successive */
	
	/* Over the boundary or cannot move,so don't move */
	for(int i=0;i<ROWSIZE;++i)
		for(int j=0;j<COLSIZE;++j){
			if(isActive(i,j)){
				if(outOfRange(i,j-1))
					return;
				if(isStable(i,j-1))							
					return;																													
			}
		}
	/* If movement is valid */	
	for(int i=0;i<ROWSIZE;++i)
		for(int j=0;j<COLSIZE;++j){
			if(isActive(i,j)){						
				box[i][j]=BLANK;
				box[i][j-1]=ACTIVE;																									
			}
		}
	--col;																	//Move to left grid
	
	/* If move to left is the last operation,then set the graph stable */
	if(keepFalling()==false)
		stableSetUp();
}

void Container::rightToMove(){
	/* Reverse search from top to bottom,right to left */
	/* To avoid the situation which two grid are successive */
	
	/* Over the boundary or cannot move,so don't move */
	for(int i=0;i<ROWSIZE;++i)
		for(int j=COLSIZE-1;j>-1;--j){				
			if(isActive(i,j)){
				if(outOfRange(i,j+1))
					return;
				if(isStable(i,j+1))				
					return;						
			}
		}
	/* If movement is valid */
	for(int i=0;i<ROWSIZE;++i)
		for(int j=COLSIZE-1;j>-1;--j){				
			if(isActive(i,j)){					
				box[i][j]=BLANK;
				box[i][j+1]=ACTIVE;
			}
		}
	++col;																	//Move to right grid
	
	/* If move to right is the last operation,then set the graph stable */
	if(keepFalling()==false)
		stableSetUp();
}

void Container::upToTransform(){
	switch(graphIndex){
		case 1:
			g1Up();
			break;
		case 2:
			g2Up();
			break;
		case 3:
			g3Up();
			break;
		case 4:
			g4Up();
			break;
		case 5:
			g5Up();
			break;
		case 6:
			g6Up();
			break;
		case 7:
			g7Up();
			break;				
	}
}

void Container::g1Up(){
	if(status==0){													//Transform status 0 to status 1
		if(outOfRange(row,col+1)||outOfRange(row-1,col-2)||outOfRange(row-1,col-1)		//Cannot transform when over the boundary
				||outOfRange(row-1,col+1)||outOfRange(row-2,col-2)||outOfRange(row-2,col-1)||outOfRange(row-2,col))
			return;
		if(isStable(row,col+1)||isStable(row-1,col-2)||isStable(row-1,col-1)					//Cannot transform in this situation
				||isStable(row-1,col+1)||isStable(row-2,col-2)||isStable(row-2,col-1)||isStable(row-2,col))
			return;																		
		box[row][col]=box[row][col-2]=box[row][col-1]=box[row-1][col]=BLANK;
		box[row][col]=box[row-1][col]=box[row-2][col]=box[row][col+1]=ACTIVE;
	}
	if(status==1){													//Transform status 1 to status 2
		if(outOfRange(row+1,col)||outOfRange(row+1,col+1)||outOfRange(row,col+2)			//Cannot transform when over the boundary
				||outOfRange(row-1,col+1)||outOfRange(row-1,col+2)||outOfRange(row-2,col+1)||outOfRange(row-2,col+2))
			return;
		if(isStable(row+1,col)||isStable(row+1,col+1)||isStable(row,col+2)						//Cannot transform in this situation
				||isStable(row-1,col+1)||isStable(row-1,col+2)||isStable(row-2,col+1)||isStable(row-2,col+2))
			return;
		box[row][col]=box[row-1][col]=box[row-2][col]=box[row][col+1]=BLANK;
		++row;																//Reset the base position
		box[row][col]=box[row-1][col]=box[row-1][col+1]=box[row-1][col+2]=ACTIVE;
	}
	if(status==2){													//Transform status 2 to status 3
		if(outOfRange(row+1,col)||outOfRange(row+1,col+1)||outOfRange(row+1,col+2)		//Cannot transform when over the boundary
				||outOfRange(row,col-1)||outOfRange(row,col+1)||outOfRange(row,col+2)||outOfRange(row-1,col-1))
			return;
		if(isStable(row+1,col)||isStable(row+1,col+1)||isStable(row+1,col+2)					//Cannot transform in this situation
				||isStable(row,col-1)||isStable(row,col+1)||isStable(row,col+2)||isStable(row-1,col-1))
			return;
		box[row][col]=box[row-1][col]=box[row-1][col+1]=box[row-1][col+2]=BLANK;
		++row;																//Reset the base position
		box[row][col]=box[row-1][col]=box[row-2][col]=box[row-2][col-1]=ACTIVE;
	}
	if(status==3){													//Transform status 3 to status 0
		if(outOfRange(row,col-1)||outOfRange(row,col-2)||outOfRange(row-1,col-1)			//Cannot transform when over the boundary
				||outOfRange(row-1,col-2)||outOfRange(row-2,col-2)||outOfRange(row-3,col)||outOfRange(row-3,col-1))
			return;
		if(isStable(row,col-1)||isStable(row,col-2)||isStable(row-1,col-1)						//Cannot transform in this situation
				||isStable(row-1,col-2)||isStable(row-2,col-2)||isStable(row-3,col)||isStable(row-3,col-1))
			return;
		box[row][col]=box[row-1][col]=box[row-2][col]=box[row-2][col-1]=BLANK;
		row-=2;																//Reset the base position
		box[row][col]=box[row][col-2]=box[row][col-1]=box[row-1][col]=ACTIVE;
	}
	++status;																//Update satus information
	if(status==4)
		status=0;
}

void Container::g2Up(){
	if(status==0){													//Transform status 0 to status 1
		if(outOfRange(row-1,col+1)||outOfRange(row+1,col)||outOfRange(row+1,col+1)		//Cannot transform when over the boundary
				||outOfRange(row+1,col+3)||outOfRange(row+2,col)||outOfRange(row+2,col+1)||outOfRange(row+2,col+3))
			return;
		if(isStable(row-1,col+1)||isStable(row+1,col)||isStable(row+1,col+1)					//Cannot transform in this situation
				||isStable(row+1,col+3)||isStable(row+2,col)||isStable(row+2,col+1)||isStable(row+2,col+3))
			return;																		
		box[row][col]=box[row][col+1]=box[row][col+2]=box[row-1][col]=BLANK;
		row+=2;																//Reset the base position
		box[row][col]=box[row-1][col]=box[row-2][col]=box[row-2][col+1]=ACTIVE;
	}
	if(status==1){													//Transform status 1 to status 2
		if(outOfRange(row,col-1)||outOfRange(row,col-2)||outOfRange(row-1,col+1)			//Cannot transform when over the boundary
				||outOfRange(row-1,col-1)||outOfRange(row-1,col-2)||outOfRange(row-2,col-2)||outOfRange(row-2,col-1))
			return;
		if(isStable(row,col-1)||isStable(row,col-2)||isStable(row-1,col+1)						//Cannot transform in this situation
				||isStable(row-1,col-1)||isStable(row-1,col-2)||isStable(row-2,col-2)||isStable(row-2,col-1))
			return;
		box[row][col]=box[row-1][col]=box[row-2][col]=box[row-2][col+1]=BLANK;
		--row;																//Reset the base position
		box[row][col]=box[row-1][col-2]=box[row-1][col-1]=box[row-1][col]=ACTIVE;
	}
	if(status==2){													//Transform status 2 to status 3
		if(outOfRange(row,col-1)||outOfRange(row-2,col)||outOfRange(row-2,col-1)		//Cannot transform when over the boundary
				||outOfRange(row-2,col-2)||outOfRange(row-3,col)||outOfRange(row-3,col-1)||outOfRange(row-3,col-2))
			return;
		if(isStable(row,col-1)||isStable(row-2,col)||isStable(row-2,col-1)					//Cannot transform in this situation
				||isStable(row-2,col-2)||isStable(row-3,col)||isStable(row-3,col-1)||isStable(row-3,col-2))
			return;
		box[row][col]=box[row-1][col-2]=box[row-1][col-1]=box[row-1][col]=BLANK;
		--row;																//Reset the base position
		box[row][col]=box[row][col-1]=box[row-1][col]=box[row-2][col]=ACTIVE;
	}
	if(status==3){													//Transform status 3 to status 0
		if(outOfRange(row,col+1)||outOfRange(row,col+2)||outOfRange(row-1,col-1)			//Cannot transform when over the boundary
				||outOfRange(row-1,col+1)||outOfRange(row-1,col+2)||outOfRange(row-2,col+1)||outOfRange(row-2,col+2))
			return;
		if(isStable(row,col+1)||isStable(row,col+2)||isStable(row-1,col-1)						//Cannot transform in this situation
				||isStable(row-1,col+1)||isStable(row-1,col+2)||isStable(row-2,col+1)||isStable(row-2,col+2))
			return;
		box[row][col]=box[row][col-1]=box[row-1][col]=box[row-2][col]=BLANK;
		box[row][col]=box[row][col+1]=box[row][col+2]=box[row-1][col]=ACTIVE;
	}
	++status;																//Update satus information
	if(status==4)
		status=0;
}

void Container::g3Up(){
																					//Graph3 has only one status,so do nothing
}

void Container::g4Up(){
	if(status==0){													//Transform status 0 to status 1				
		if(outOfRange(row,col-1)||outOfRange(row-1,col-1)||outOfRange(row-2,col-1)||outOfRange(row-2,col))
			return;
		if(isStable(row,col-1)||isStable(row-1,col-1)||isStable(row-2,col-1)||isStable(row-2,col))
			return;
		box[row][col]=box[row-1][col]=box[row-1][col+1]=box[row-2][col+1]=BLANK;
		--row;																//Reset the base position
		++col;																//Reset the base position
		box[row][col]=box[row][col-1]=box[row-1][col-1]=box[row-1][col-2]=ACTIVE;
	}
	if(status==1){													//Transform status 1 to status 0
		if(outOfRange(row-1,col)||outOfRange(row-2,col-2)||outOfRange(row-2,col-1)||outOfRange(row-2,col))
			return;
		if(isStable(row-1,col)||isStable(row-2,col-2)||isStable(row-2,col-1)||isStable(row-2,col))
			return;
		box[row][col]=box[row][col-1]=box[row-1][col-1]=box[row-1][col-2]=BLANK;
		--col;																//Reset the base position
		box[row][col]=box[row-1][col]=box[row-1][col+1]=box[row-2][col+1]=ACTIVE;
	}
	++status;																//Update satus information
	if(status==2)
		status=0;
}

void Container::g5Up(){
	if(status==0){													//Transform status 0 to status 1				
		if(outOfRange(row,col-1)||outOfRange(row-2,col)||outOfRange(row-2,col+1))
			return;
		if(isStable(row,col-1)||isStable(row-2,col)||isStable(row-2,col+1))
			return;
		box[row][col]=box[row-1][col]=box[row-1][col-1]=box[row-2][col-1]=BLANK;
		--row;																//Reset the base position
		--col;																//Reset the base position
		box[row][col]=box[row][col+1]=box[row-1][col+1]=box[row-1][col+2]=ACTIVE;
	}
	if(status==1){													//Transform status 1 to status 0
		if(outOfRange(row,col+2)||outOfRange(row-1,col)||outOfRange(row-2,col))
			return;
		if(isStable(row,col+2)||isStable(row-1,col)||isStable(row-2,col))
			return;
		box[row][col]=box[row][col+1]=box[row-1][col+1]=box[row-1][col+2]=BLANK;
		++col;																//Reset the base position
		box[row][col]=box[row-1][col]=box[row-1][col-1]=box[row-2][col-1]=ACTIVE;
	}
	++status;																//Update satus information
	if(status==2)
		status=0;
}

void Container::g6Up(){
	if(status==0){													//Transform status 0 to status 1				
		if(outOfRange(row+1,col+2)||outOfRange(row+1,col+3)||outOfRange(row-1,col)||outOfRange(row-1,col+1)
				||outOfRange(row-1,col+2)||outOfRange(row-2,col)||outOfRange(row-2,col+1)||outOfRange(row-2,col+2))
			return;
		if(isStable(row+1,col+2)||isStable(row+1,col+3)||isStable(row-1,col)||isStable(row-1,col+1)					
				||isStable(row-1,col+2)||isStable(row-2,col)||isStable(row-2,col+1)||isStable(row-2,col+2))
			return;
		box[row][col]=box[row][col+1]=box[row][col+2]=box[row][col+3]=BLANK;
		++row;																//Reset the base position
		col+=2;																//Reset the base position
		box[row][col]=box[row-1][col]=box[row-2][col]=box[row-3][col]=ACTIVE;
	}
	if(status==1){													//Transform status 1 to status 0
		if(outOfRange(row,col+1)||outOfRange(row-1,col-2)||outOfRange(row-1,col-1)||outOfRange(row-1,col+1)
				||outOfRange(row-2,col-2)||outOfRange(row-2,col-1)||outOfRange(row-3,col-2)||outOfRange(row-3,col-1))
			return;
		if(isStable(row,col+1)||isStable(row-1,col-2)||isStable(row-1,col-1)||isStable(row-1,col+1)					
				||isStable(row-2,col-2)||isStable(row-2,col-1)||isStable(row-3,col-2)||isStable(row-3,col-1))
			return;
			
		box[row][col]=box[row-1][col]=box[row-2][col]=box[row-3][col]=BLANK;
		--row;																//Reset the base position
		col-=2;																//Reset the base position
		box[row][col]=box[row][col+1]=box[row][col+2]=box[row][col+3]=ACTIVE;
	}
	++status;																//Update satus information
	if(status==2)
		status=0;
}

void Container::g7Up(){
	if(status==0){													//Transform status 0 to status 1
		if(outOfRange(row+1,col)||outOfRange(row+1,col+1)||outOfRange(row-1,col-1)||outOfRange(row-1,col+1))
			return;
		if(isStable(row+1,col)||isStable(row+1,col+1)||isStable(row-1,col-1)||isStable(row-1,col+1))
			return;
		box[row][col]=box[row][col-1]=box[row][col+1]=box[row-1][col]=BLANK;
		++row;																//Reset the base position
		box[row][col]=box[row-1][col]=box[row-1][col+1]=box[row-2][col]=ACTIVE;
	}
	if(status==1){													//Transform status 1 to status 2
		if(outOfRange(row,col-1)||outOfRange(row,col+1)||outOfRange(row-1,col-1)||outOfRange(row-2,col+1))
			return;
		if(isStable(row,col-1)||isStable(row,col+1)||isStable(row-1,col-1)||isStable(row-2,col+1))
			return;
		box[row][col]=box[row-1][col]=box[row-1][col+1]=box[row-2][col]=BLANK;
		box[row][col]=box[row-1][col-1]=box[row-1][col]=box[row-1][col+1]=ACTIVE;
	}
	if(status==2){													//Transform status 2 to status 3
		if(outOfRange(row,col-1)||outOfRange(row,col+1)||outOfRange(row-2,col-1)||outOfRange(row-2,col))
			return;
		if(isStable(row,col-1)||isStable(row,col+1)||isStable(row-2,col-1)||isStable(row-2,col))
			return;
		box[row][col]=box[row-1][col-1]=box[row-1][col]=box[row-1][col+1]=BLANK;
		box[row][col]=box[row-1][col-1]=box[row-1][col]=box[row-2][col]=ACTIVE;
	}
	if(status==3){													//Transform status 3 to status 0
		if(outOfRange(row,col-1)||outOfRange(row-1,col+1)||outOfRange(row-2,col-1)||outOfRange(row-2,col+1))
			return;
		if(isStable(row,col-1)||isStable(row-1,col+1)||isStable(row-2,col-1)||isStable(row-2,col+1))
			return;
		box[row][col]=box[row-1][col-1]=box[row-1][col]=box[row-2][col]=BLANK;
		--row;																//Reset the base position
		box[row][col]=box[row][col-1]=box[row][col+1]=box[row-1][col]=ACTIVE;
	}
	++status;																//Update satus information
	if(status==4)
		status=0;
}

void Container::natureDown(){
	/* If we didn't hit keyboard,then the graph down naturally */
	/* Reverse search from bottom to top,right to left */
	/* To avoid the situation which two grid are successive */
	for(int i=ROWSIZE-1;i>-1;--i)
		for(int j=COLSIZE-1;j>-1;--j)
			if(isActive(i,j)){				
				if(outOfRange(i+1,j))
					return;
				if(isStable(i+1,j))
					return;
				box[i][j]=BLANK;
				box[i+1][j]=ACTIVE;
			}
			
	++row;												//Fall to next row
	if(keepFalling()==false)			//If natural down is the last operation then set the graph stable
		stableSetUp();
}

bool Container::keepFalling(){
	if(isStable(row,col))														//Already stable,stop falling,if ignore this
		return false;																	//next time the graph will still falling coz all grids are stable
																									//and then the condition of keep falling will be true,so falling goes on
	if(isBottom())
		return false;
	
	for(int i=0;i<ROWSIZE;i++)
		for(int j=0;j<COLSIZE;j++)
			if(isActive(i,j)){													//To avoid over the boundary
				if(outOfRange(i+1,j))
					return false;
				if(isStable(i+1,j))
					return false;
			}
	
	return true;
}

void Container::spaceToFallDirectly(){
	row=findDownRow();
	cleanActive();
	
	/* Set arrived graph as stable graph */
	stableSetUp();
}


int Container::findDownRow(){
	if(isBottom()==false){
		int minDistance=ROWSIZE;
		for(int i=0;i<ROWSIZE;++i)
			for(int j=0;j<ROWSIZE;++j)
				if(isActive(i,j)){														//Find the active graph's grid
					for(int k=i+1;k<ROWSIZE;++k)								//and calculate each grid's distance to vertical stable grid or the
						if(isStable(k,j)){												//bottom of the container,and the minimum of all the distance is the
							int distance=k-i-1;											//distance the graph should fall
							if(minDistance>distance)								
								minDistance=distance;										
							break;
						}
				}
		if(outOfRange(row+minDistance,col)==false)				//If there is no stable grid in vertical orientation
			return row+minDistance;													//or the the new row we got over the boundary
	}																										//means all the grids in the vertical of the base position are blank
	return ROWSIZE-1;																		//so just fall to the bottom of the container
}

void Container::g1Stable(){
	if(status==0)
		box[row][col]=box[row][col-2]=box[row][col-1]=box[row-1][col]=STABLE;
	if(status==1)
		box[row][col]=box[row-1][col]=box[row-2][col]=box[row][col+1]=STABLE;
	if(status==2)
		box[row][col]=box[row-1][col]=box[row-1][col+1]=box[row-1][col+2]=STABLE;
	if(status==3)
		box[row][col]=box[row-1][col]=box[row-2][col]=box[row-2][col-1]=STABLE;
}

void Container::g2Stable(){
	if(status==0)
		box[row][col]=box[row][col+1]=box[row][col+2]=box[row-1][col]=STABLE;
	if(status==1)
		box[row][col]=box[row-1][col]=box[row-2][col]=box[row-2][col+1]=STABLE;
	if(status==2)
		box[row][col]=box[row-1][col-2]=box[row-1][col-1]=box[row-1][col]=STABLE;
	if(status==3)
		box[row][col]=box[row][col-1]=box[row-1][col]=box[row-2][col]=STABLE;
}

void Container::g3Stable(){
	if(status==0)
		box[row][col]=box[row][col-1]=box[row-1][col]=box[row-1][col-1]=STABLE;
}

void Container::g4Stable(){
	if(status==0)
		box[row][col]=box[row-1][col]=box[row-1][col+1]=box[row-2][col+1]=STABLE;
	if(status==1)
		box[row][col]=box[row][col-1]=box[row-1][col-1]=box[row-1][col-2]=STABLE;
}

void Container::g5Stable(){
	if(status==0)
		box[row][col]=box[row-1][col]=box[row-1][col-1]=box[row-2][col-1]=STABLE;
	if(status==1)
		box[row][col]=box[row][col+1]=box[row-1][col+1]=box[row-1][col+2]=STABLE;
}

void Container::g6Stable(){
	if(status==0)
		box[row][col]=box[row][col+1]=box[row][col+2]=box[row][col+3]=STABLE;
	if(status==1)
		box[row][col]=box[row-1][col]=box[row-2][col]=box[row-3][col]=STABLE;
}

void Container::g7Stable(){
	if(status==0)
		box[row][col]=box[row][col-1]=box[row][col+1]=box[row-1][col]=STABLE;
	if(status==1)
		box[row][col]=box[row-1][col]=box[row-1][col+1]=box[row-2][col]=STABLE;
	if(status==2)
		box[row][col]=box[row-1][col-1]=box[row-1][col]=box[row-1][col+1]=STABLE;
	if(status==3)
		box[row][col]=box[row-1][col-1]=box[row-1][col]=box[row-2][col]=STABLE;
}

void Container::show(){
	switch(graphIndex){
		case 1:
			g1Active();
			break;
		case 2:
			g2Active();
			break;
		case 3:
			g3Active();
			break;
		case 4:
			g4Active();
			break;
		case 5:
			g5Active();
			break;
		case 6:
			g6Active();
			break;
		case 7:
			g7Active();
			break;
	}
	
	while(true){
		backToBegin();												//Back to the beginning of the screen
		staticShow();													//and show the falling of the new graph
		if(keepFalling()==false){
			stableSetUp();											//If the graph gonna stop in the initial row,then set it stable is vital for breaking
			break;
		}
		
		if(!kbhit()){
			for(time_t now=clock();clock()-now<CLOCKS_PER_SEC/speedFactor;	);
			natureDown();
		}
		else{
			int ck1=getch();
			if(ck1==0x20)												//If it's space,then fall down directly
				spaceToFallDirectly();
			else{
				int ck2=getch();
				getCommand(ck2);
			}
		}
	}
}

void Container::staticShow(){
	for(int i=0;i<ROWSIZE;i++){
			cout<<"                         ";
			cout<<BORDER;
			for(int j=0;j<COLSIZE;j++){
				if(box[i][j]==BLANK)
					cout<<" ";
				else															//When it's active or stable grid,output the grid			
					cout<<GRID;
			}
			cout<<BORDER<<endl;
		}
		cout<<"                                                   ";
		cout<<"current scores: "<<score<<endl;
}

void Container::g1Active(){
	/* Initialization,each status has different basic position to fix a graph,this basic position is in the bottom of the graph */
	if(status==0)
		box[row][col]=box[row][col-2]=box[row][col-1]=box[row-1][col]=ACTIVE;
	if(status==1)
		box[row][col]=box[row-1][col]=box[row-2][col]=box[row][col+1]=ACTIVE;
	if(status==2)
		box[row][col]=box[row-1][col]=box[row-1][col+1]=box[row-1][col+2]=ACTIVE;
	if(status==3)
		box[row][col]=box[row-1][col]=box[row-2][col]=box[row-2][col-1]=ACTIVE;
}

void Container::g2Active(){
	if(status==0)
		box[row][col]=box[row][col+1]=box[row][col+2]=box[row-1][col]=ACTIVE;
	if(status==1)
		box[row][col]=box[row-1][col]=box[row-2][col]=box[row-2][col+1]=ACTIVE;
	if(status==2)
		box[row][col]=box[row-1][col-2]=box[row-1][col-1]=box[row-1][col]=ACTIVE;
	if(status==3)
		box[row][col]=box[row][col-1]=box[row-1][col]=box[row-2][col]=ACTIVE;
}

void Container::g3Active(){
	if(status==0)
		box[row][col]=box[row][col-1]=box[row-1][col]=box[row-1][col-1]=ACTIVE;
}

void Container::g4Active(){
	if(status==0)
		box[row][col]=box[row-1][col]=box[row-1][col+1]=box[row-2][col+1]=ACTIVE;
	if(status==1)
		box[row][col]=box[row][col-1]=box[row-1][col-1]=box[row-1][col-2]=ACTIVE;
}

void Container::g5Active(){
	if(status==0)
		box[row][col]=box[row-1][col]=box[row-1][col-1]=box[row-2][col-1]=ACTIVE;
	if(status==1)
		box[row][col]=box[row][col+1]=box[row-1][col+1]=box[row-1][col+2]=ACTIVE;
}

void Container::g6Active(){
	if(status==0)
		box[row][col]=box[row][col+1]=box[row][col+2]=box[row][col+3]=ACTIVE;
	if(status==1)
		box[row][col]=box[row-1][col]=box[row-2][col]=box[row-3][col]=ACTIVE;
}

void Container::g7Active(){
	if(status==0)
		box[row][col]=box[row][col-1]=box[row][col+1]=box[row-1][col]=ACTIVE;
	if(status==1)
		box[row][col]=box[row-1][col]=box[row-1][col+1]=box[row-2][col]=ACTIVE;
	if(status==2)
		box[row][col]=box[row-1][col-1]=box[row-1][col]=box[row-1][col+1]=ACTIVE;
	if(status==3)
		box[row][col]=box[row-1][col-1]=box[row-1][col]=box[row-2][col]=ACTIVE;
}

void backToBegin(){
	COORD scrn;
  HANDLE hOuput=GetStdHandle(STD_OUTPUT_HANDLE);
  scrn.X=0;
	scrn.Y=0;
  SetConsoleCursorPosition(hOuput,scrn);
}
